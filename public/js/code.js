document.addEventListener("DOMContentLoaded", function (event) {
    console.log("Dom loaded")
});
//Функция калькулятор
function raschitat() {
    kolvo = Number(document.getElementById('kol-vo').value);
    price = Number(document.getElementById('price').value);
    mesac = Number(document.getElementById('mesac').value);
    if (kolvo == "") {
        alert("Вы не указали кол-во товара");
    } else if (price == "") {
        alert("Вы не указали цену за 1 штуку");
    }else {
        proisvedenie = parseFloat(kolvo) * parseFloat(price);
        if(mesac ==""){
            summa = proisvedenie;
        } else {
            summa = proisvedenie / mesac;
        }
        if(Number.isNaN(proisvedenie) || Number.isNaN(summa) ){
            document.getElementById('proisvedenie').innerHTML = "Некорректный ввод данных.";
            document.getElementById('summa').innerHTML = "Используйте только цифры от 0 до 9.";
        }else{
            document.getElementById('proisvedenie').innerHTML = "Итоговая цена составит: " + proisvedenie + " рублей";
            document.getElementById('summa').innerHTML = "В месяц вам нужно платить: " + summa +" рублей, на протяжении " + mesac + " месяцев";
        }
    }
}

// Скрипт для подсветки обязательных полей красным
(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();